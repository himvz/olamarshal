package com.ola.consumer.ui.fragment;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimatedStateListDrawable;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import mytaxi.com.ola.R;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.ola.consumer.map.MapUtil;
import com.ola.consumer.models.BookingDetails;
import com.ola.consumer.models.LocationP;
import com.ola.consumer.services.DemoCamService;
import com.ola.consumer.services.GeofenceTransitionsIntentService;
import com.ola.consumer.ui.MainActivity;
import com.ola.sdk.deviceplatform.tracking.manager.SocketFacade;

/**
 * @author Waseem Mirza
 */

public class VehicleMapFragment extends SupportMapFragment implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, MapUtil.OnUserActionListener/*, ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener*/ {

    private static final String TAG = "VehicleMapFragment";

    private GeofencingClient mGeofencingClient;

    private List<Geofence> mGeofenceList = new ArrayList<>();

    private PendingIntent mGeofencePendingIntent;


    private GoogleMap mMap;
    private MainActivity mActivity;

    private GoogleApiClient mGoogleApiClient;

    private MapUtil mMaputil;

    private LocationRequest mLocationRequest;

    private Location mLastLocation;

    private VideoBroadcastReceiver mVideoReceiver;

    private View mBookButton, mChooseButton;

    private Marker mCurrLocationMarker;

    private Timer mTimer;

    private UpdateTimerTask mUpdateTimerTask;
    private ArrayList<LatLng> mCheckPoints;
    private Set<String> path;



    //private LatLng mDropPoint  = new LatLng(13.028189, 77.550601);
    private LatLng mDropPoint = new LatLng(12.981858, 77.594203);
    private LatLng mPickupPoint = new LatLng(13.028189, 77.550601);




    public VehicleMapFragment() {
    }

    public static VehicleMapFragment newInstance(LatLng latLng) {
        VehicleMapFragment fragment = new VehicleMapFragment();
        if (latLng != null) {
            Bundle args = new Bundle();
            args.putDouble("lat", latLng.latitude);
            args.putDouble("lng", latLng.longitude);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = super.onCreateView(layoutInflater, viewGroup, bundle);
        FrameLayout container = (FrameLayout) layoutInflater.inflate(R.layout.fragment_map_container, viewGroup, false);

        mBookButton = container.findViewById(R.id.btn_book_cab);
        mChooseButton = container.findViewById(R.id.btn_choose_route);
        mBookButton.setVisibility(View.GONE);

        mChooseButton.setVisibility(View.GONE);
        mChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                if(mMaputil != null) {
                    mMaputil.chooseRoute(mPickupPoint, mDropPoint);
                } else {
                    Snackbar.make(getView(), "Something Went Wrong", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        mBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBookButton.setVisibility(View.GONE);
                Snackbar.make(getView(),"Choosing Safest Route . . .", Snackbar.LENGTH_LONG).show();
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(mMaputil != null) {
                            mMaputil.confirmBooking();
                        } else {
                            Snackbar.make(getView(),"Something went wrong", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }, 1500);

            }
        });
        container.addView(view);
        initFabButton(container);
        initMap();
        mGeofencingClient = LocationServices.getGeofencingClient(getActivity());
        return container;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        mActivity = (MainActivity) getActivity();
        mVideoReceiver = new VideoBroadcastReceiver(mActivity);

    }

    /*
     * initialize floating action button and setup a click listener
     */
    private void initFabButton(View v) {
        FloatingActionButton fab = v.findViewById(R.id.fab_map);
        fab.setVisibility(View.GONE);
        // for kitkat without bringToFront the button doesn't appear
        fab.bringToFront();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mActivity != null) {
                    mActivity.showList();
                }
            }
        });
    }

    public void initMap() {
        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {

            mChooseButton.setVisibility(View.VISIBLE);

            mMap.setIndoorEnabled(false);
            mMap.setMaxZoomPreference(17);
            mMap.setTrafficEnabled(false);

            UiSettings mapSettings = mMap.getUiSettings();
//
//            /*
//             * Disable controls that are not required
//             */
            mapSettings.setZoomControlsEnabled(true);
            mapSettings.setCompassEnabled(true);
            mapSettings.setRotateGesturesEnabled(false);
            mapSettings.setTiltGesturesEnabled(false);
            mapSettings.setMyLocationButtonEnabled(true);

            //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(HAMBURG.getCenter(), 10));
            mMaputil = new MapUtil(mMap, this);
            mMaputil.init();
            mTimer = new Timer();
            mUpdateTimerTask = new UpdateTimerTask(new BookingDetails("CRN 3234567876", "Himanshu","Waseem","KA 01AL 2345", "","","Prime Sedan",null, null, null ));
            mTimer.scheduleAtFixedRate(mUpdateTimerTask, 1000L, 1000L);

            //mMap.setOnMarkerClickListener(this);

//            if (mActivity != null) {
//                addMarkers(mActivity.getVehicleList());
//            }

            Bundle args = getArguments();
            if (args != null) {
                double lat = args.getDouble("lat");
                double lng = args.getDouble("lng");
                if (lat > 0 && lng > 0) {
                    Log.d(TAG, "lat: " + lat + " lng: " + lng);
                    zoom(new LatLng(lat, lng));
                }
            }
        }
    }


    private void setGeofence(ArrayList<LatLng> checkpoints) {

        if (checkpoints != null) {

            for (int i = 0; i < checkpoints.size(); i++) {

                mGeofenceList.add(new Geofence.Builder()
                        // Set the request ID of the geofence. This is a string to identify this
                        // geofence.
                        .setRequestId(String.valueOf(i))

                        .setCircularRegion(
                                checkpoints.get(i).latitude,
                                checkpoints.get(i).longitude,
                                50
                        )
                        .setExpirationDuration(TimeUnit.MINUTES.toMillis(5))
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .build());
            }
        }
    }

    private void addGeoFence() {
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            com.ola.sdk.deviceplatform.common.base.utils.Logger.e("geofence added");
                        }
                    })
                    .addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            com.ola.sdk.deviceplatform.common.base.utils.Logger.e("geofence add failed");
                        }
                    });
        } catch (Exception ignored){}

    }

    private void removeGeofence(){
        try {
            mGeofencingClient.removeGeofences(getGeofencePendingIntent())
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            com.ola.sdk.deviceplatform.common.base.utils.Logger.e("geofence removed");
                        }
                    })
                    .addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            com.ola.sdk.deviceplatform.common.base.utils.Logger.e("geofence remove failed");
                        }
                    });
        } catch (Exception ignored){

        }
    }

    private GeofencingRequest getGeofencingRequest() {
        if(mGeofenceList != null && mGeofenceList.size() > 0) {
            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
            builder.addGeofences(mGeofenceList);
            return builder.build();
        }
        return null;
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(getActivity(), GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(getActivity(), 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 17));
        return true;
    }

    public void zoom(LatLng latLng) {
        Log.d(TAG, "zoom");
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    @Override
    public void updateRoute(Set<String> route) {
        path = route;
        if(route == null || route.size() == 0){
            mUpdateTimerTask.cancel();
            mTimer.cancel();
            mTimer.purge();
            mBookButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateCheckpoints(ArrayList<LatLng> checpoints) {
        mCheckPoints = checpoints;
        if(checpoints == null || checpoints.size() == 0) {
            try {
                mUpdateTimerTask.cancel();
                mTimer.cancel();
                mTimer.purge();
                mBookButton.setVisibility(View.GONE);
            } catch (Exception ignored) {}
        } else {
            mTimer = new Timer();

            mUpdateTimerTask = new UpdateTimerTask(new BookingDetails("CRN 3234567876", "Himanshu","Waseem","KA 01AL 2345", "","","Prime Sedan",new LocationP(checpoints.get(0).latitude, checpoints.get(0).longitude), new LocationP(checpoints.get(checpoints.size()-1).latitude, checpoints.get(checpoints.size()-1).longitude), null ));
            try {
                mTimer.scheduleAtFixedRate(mUpdateTimerTask, 1000L, 1000L);
                registerVideoReciever();
                SocketFacade.getInstance().subscribe("capture-image");
                SocketFacade.getInstance().subscribe("stream-video");
            } catch (Exception ignored){}
            removeGeofence();
            setGeofence(checpoints);
            addGeoFence();
        }

    }

    @Override
    public void onBookingReady() {
        mBookButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void noRouteFound() {
        if(mMap != null) {
            mMap.clear();
        }
        Snackbar.make(getView(),"No Route Found",Snackbar.LENGTH_SHORT).show();
    }


    private class UpdateTimerTask extends TimerTask{

        private BookingDetails details;

        UpdateTimerTask(BookingDetails details){
            this.details = details;
        }

        int nextCheckpoint = 0;
        @Override
        public void run() {
            onLocationChanged(SocketFacade.getInstance().getLocation(), nextCheckpoint, mCheckPoints, path, details);
        }

        private void setCheckpoint(int point) {
            nextCheckpoint = point;
        }
    }


    public void onLocationChanged(final Location location, final int next, final ArrayList<LatLng> checkpoints, final Set<String> path, final BookingDetails booking) {
        if(location == null){
            return;
        }
        mLastLocation = location;


        //move map camera
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {


                String currLoc = String.format("%.3f", location.getLatitude())+","+ String.format("%.3f", location.getLongitude());

                final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());



                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Current Position");
                if( checkpoints!= null && checkpoints.size() > 0) {
                    int curr = next;
                    if(next == -1){
                        curr = 0;
                    }
                    if(curr >= checkpoints.size()){
                        curr = checkpoints.size()-1;
                        stopTrip();
                        return;

                    }
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_car));
                    markerOptions.rotation(location.getBearing());
                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                    double dis = distance(latLng.latitude, latLng.longitude, checkpoints.get(curr).latitude, checkpoints.get(curr).longitude);

                    if(dis < 0.150){
                        //Snackbar.make(getView(), "Checkpoint" + (curr+1)+ "passed" , Snackbar.LENGTH_LONG).show();
                        showSnackbar(getView(),"CHECKPOINT " + (curr+1)+ " PASSED");
                        setCheckpoint(curr+1);
                    } else if(path != null && path.size() > 0 && path.contains(currLoc)){
                        //Snackbar.make(getView(), "On path", Snackbar.LENGTH_SHORT).show();
                        showSnackbar(getView(),"ON PATH");
                    }
                    else if(dis > 1){
                        //getActivity().startService(new Intent(getActivity(), DemoCamService.class));
                        //Snackbar.make(getView(),"Possible Deviation", Toast.LENGTH_SHORT).show();
                        showSnackbar(getView(),"POSSIBLE DEVIATION");

                        booking.setCurrent(new LocationP(location.getLatitude(), location.getLongitude()));
                        booking.setAlertN("Deviation");
                        booking.setAlertType("Deviation");


                        SocketFacade.getInstance().publishMessages("safety", new String("alert|"+new Gson().toJson(booking)).getBytes());
                    }
                    else  {
                        //Toast.makeText(getActivity(), "Next CheckPoint in " + dis + " km", Toast.LENGTH_SHORT).show();
                        setCheckpoint(curr);
                    }

                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    // mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                } else {
                    setCheckpoint(-1);
                }
            }

            private void showSnackbar(View containerLayout,String text) {

                Snackbar snackbar = Snackbar.make(containerLayout, "", Snackbar.LENGTH_SHORT);
                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();

                //textView.setVisibility(View.INVISIBLE);

                Context context = getContext();
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View snackView = mInflater.inflate(R.layout.snapper_view, null);
                TextView textView = (TextView) layout.findViewById(R.id.text);
                //ImageView imageView = (ImageView) snackView.findViewById(R.id.image);
                //imageView.setImageBitmap(image);
                TextView textViewTop = (TextView) snackView.findViewById(R.id.text);
                textViewTop.setText(text);
                //textViewTop.setTextColor(Color.WHITE);

                layout.addView(snackView, 0);
                snackbar.show();
            }
        });


//        //stop location updates
//        if (mGoogleApiClient != null) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//        }

    }

    private void stopTrip() {
        try {
            mUpdateTimerTask.cancel();
            mTimer.cancel();
            mTimer.purge();

            unregisterVideoReciever();

        } catch (Exception ignored) {}
        finally {
            Snackbar.make(getView(),"Your Trip has ended.",Snackbar.LENGTH_SHORT).show();
        }
    }

    private void registerVideoReciever() {

        unregisterVideoReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.android.RECORD_VIDEO");
        if(mVideoReceiver == null){
            mVideoReceiver = new VideoBroadcastReceiver(mActivity);
        }
        mActivity.registerReceiver(mVideoReceiver, filter);
    }

    private void unregisterVideoReciever() {
        try {

            mActivity.unregisterReceiver(mVideoReceiver);
        } catch (Exception e){

        }
    }

    private void setCheckpoint(int point) {
        mUpdateTimerTask.setCheckpoint(point);
    }


    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private static class VideoBroadcastReceiver extends BroadcastReceiver {

        MainActivity activity;

        VideoBroadcastReceiver(MainActivity activity) {
            this.activity = activity;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null && !TextUtils.isEmpty(intent.getStringExtra("message"))){
                Log.e("HIMANSHU", "");
                try {
                    if (activity != null) {
                        activity.startStream();
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    com.ola.sdk.deviceplatform.common.base.utils.Logger.e("HIMANSHU **** error starting video stream"+ e.toString());
                }
            }
        }
    }
}
