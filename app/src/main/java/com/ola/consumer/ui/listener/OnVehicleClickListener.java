package com.ola.consumer.ui.listener;


/**
 * @author Waseem Mirza
 */

public interface OnVehicleClickListener {

    void onVehicleClick(Object vehicle);
}
