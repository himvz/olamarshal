package com.ola.consumer.ui;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidhiddencamera.CameraConfig;
import com.androidhiddencamera.config.CameraFacing;
import com.androidhiddencamera.config.CameraImageFormat;
import com.androidhiddencamera.config.CameraResolution;
import com.androidhiddencamera.config.CameraRotation;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import io.kickflip.sdk.Kickflip;
import io.kickflip.sdk.api.json.Stream;
import io.kickflip.sdk.av.BroadcastListener;
import io.kickflip.sdk.exception.KickflipException;
import mytaxi.com.ola.R;
import com.ola.consumer.ui.fragment.VehicleListFragment;
import com.ola.consumer.ui.fragment.VehicleMapFragment;
import com.ola.sdk.deviceplatform.common.base.utils.Logger;
import com.ola.sdk.deviceplatform.tracking.manager.SocketFacade;

/**
 * @author Waseem Mirza
 *
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";


    private ProgressBar mLoading;
    private Button mRetryButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setLogo(R.drawable.logo);
        actionbar.setDisplayShowTitleEnabled(true);

        actionbar.setDisplayUseLogoEnabled(true);

        mLoading = findViewById(R.id.loading);
        mRetryButton = findViewById(R.id.retry_button);
        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchVehicles();
            }
        });
        addMapFragment(new LatLng(12.949881, 77.643292));
    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    private void addVehicleListFragment() {
        VehicleListFragment vehicleFragment = VehicleListFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, vehicleFragment).commitAllowingStateLoss();
    }

    private void addMapFragment(LatLng latLng) {
        VehicleMapFragment mapFragment = VehicleMapFragment.newInstance(latLng);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, mapFragment).commitAllowingStateLoss();
        transaction.addToBackStack(null);
    }

    /*
     * Get the list of vehicles for given location bounds
     */
    private void fetchVehicles() {

        Map<String, Double> queryParams = new HashMap<>();
        queryParams.put("p1Lat", 53.694865);
        queryParams.put("p1Lon", 9.757589);
        queryParams.put("p2Lat", 53.394655);
        queryParams.put("p2Lon", 10.099891);

        mRetryButton.setVisibility(View.GONE);
        mLoading.setVisibility(View.VISIBLE);
    }


    /**
     * display the vehicle list
     */
    public void showList() {
        // remove MapFragment from the stack
        // VehicleListFragment is already present in the hierarchy
        onBackPressed();
    }

    /**
     * display man and zoom on the {@link LatLng} provided, if null do not zoom
     *
     * @param latLng coordinates of the location to zoom into
     */
    public void showMapAndZoom(LatLng latLng) {
        addMapFragment(latLng);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




    public void startStream() {

        Kickflip.startBroadcastActivity(

                this, new

                        BroadcastListener() {
                            @Override
                            public void onBroadcastStart () {
                                Logger.e("HIMANSHU ***", "Camera Start");
                            }

                            @Override
                            public void onBroadcastLive (Stream stream){
                                Logger.e("HIMANSHU **** BroadcastLive @ " + stream.getKickflipUrl());
                                SocketFacade.getInstance().publishMessages("camera_stream", ("camera_stream|"+stream.getStreamUrl()).getBytes());
                            }

                            @Override
                            public void onBroadcastStop () {

                            }

                            @Override
                            public void onBroadcastError (KickflipException error){
                                Logger.e("HIMANSHU **** BroadcastLive @ error" + error.toString());
                            }

                        });
    }



}
