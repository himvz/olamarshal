package com.ola.consumer.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mytaxi.com.ola.R;
import com.ola.consumer.ui.MainActivity;
import com.ola.consumer.ui.listener.OnVehicleClickListener;

/**
 * @author Waseem Mirza
 */

public class VehicleListFragment extends Fragment implements OnVehicleClickListener {

    private static final String TAG = "HomeFragment";

    private RecyclerView mRecyclerView;
    private TextView mEmptyView;

    //private VehiclesAdapter mAdapter;

    private MainActivity mActivity;

    public VehicleListFragment() {

    }

    public static VehicleListFragment newInstance() {
        return new VehicleListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_vehicle_list, container, false);

        initViews(view);
        initFabButton(view);
        setupViews();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (MainActivity) getActivity();

//        if (mAdapter.getItemCount() == 0) {
//            mEmptyView.setVisibility(View.VISIBLE);
//        } else {
//            mEmptyView.setVisibility(View.GONE);
//        }
    }

    /*
    * initialize floating action button and setup a click listener
    */
    private void initFabButton(View v) {
        FloatingActionButton fab = v.findViewById(R.id.fab_list);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mActivity != null) {
                    mActivity.showMapAndZoom(null);
                }
            }
        });
    }

    private void initViews(View v) {
        mRecyclerView = v.findViewById(R.id.recyclerViewVehicleList);
        mEmptyView = v.findViewById(R.id.empty);
    }

    /*
     *  setup recyclerview, add layout manager and adapter
     */
    private void setupViews() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        //mAdapter = new VehiclesAdapter(this);
        //mRecyclerView.setAdapter(mAdapter);
    }

    /*
     * Callback method when user clicks an item
     * on the vehicle list
     */
    @Override
    public void onVehicleClick(Object vehicle) {
        //mActivity.showMapAndZoom();
    }

}
