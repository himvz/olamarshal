package com.ola.consumer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingDetails {

    @SerializedName("crn")
    @Expose
    private String crn;
    @SerializedName("cName")
    @Expose
    private String cName;
    @SerializedName("dName")
    @Expose
    private String dName;
    @SerializedName("vNo")
    @Expose
    private String vNo;
    @SerializedName("alertN")
    @Expose
    private String alertN;
    @SerializedName("alertType")
    @Expose
    private String alertType;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("start")
    @Expose
    private LocationP start;
    @SerializedName("stop")
    @Expose
    private LocationP stop;
    @SerializedName("current")
    @Expose
    private LocationP current;

    public BookingDetails(String crn, String cName, String dName, String vNo, String alertN, String alertType, String category, LocationP start, LocationP stop, LocationP current) {
        this.crn = crn;
        this.cName = cName;
        this.dName = dName;
        this.vNo = vNo;
        this.alertN = alertN;
        this.alertType = alertType;
        this.category = category;
        this.start = start;
        this.stop = stop;
        this.current = current;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public String getDName() {
        return dName;
    }

    public void setDName(String dName) {
        this.dName = dName;
    }

    public String getVNo() {
        return vNo;
    }

    public void setVNo(String vNo) {
        this.vNo = vNo;
    }

    public String getAlertN() {
        return alertN;
    }

    public void setAlertN(String alertN) {
        this.alertN = alertN;
    }

    public String getAlertType() {
        return alertType;
    }

    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocationP getStart() {
        return start;
    }

    public void setStart(LocationP start) {
        this.start = start;
    }

    public LocationP getStop() {
        return stop;
    }

    public void setStop(LocationP stop) {
        this.stop = stop;
    }

    public LocationP getCurrent() {
        return current;
    }

    public void setCurrent(LocationP current) {
        this.current = current;
    }

}