package com.ola.consumer.app;

import android.app.Application;
import android.content.Intent;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;
import com.ola.sdk.deviceplatform.DevicePlatform;
import com.ola.sdk.deviceplatform.common.base.analytics.AnalyticsEventListener;
import com.ola.sdk.deviceplatform.common.base.config.AppConfig;
import com.ola.sdk.deviceplatform.common.base.config.Config;
import com.ola.sdk.deviceplatform.common.base.utils.Constants;
import com.ola.sdk.deviceplatform.common.base.utils.Logger;
import com.ola.sdk.deviceplatform.network.auth.util.AuthState;
import com.ola.sdk.deviceplatform.network.manager.IAuthenticationListener;
import com.ola.sdk.deviceplatform.network.rest.init.config.NetworkConfig;
import com.ola.sdk.deviceplatform.network.rest.models.OlaRetryPolicy;
import com.ola.sdk.deviceplatform.tracking.callback.SocketCallback;
import com.ola.sdk.deviceplatform.tracking.config.SocketConfig;
import com.ola.sdk.deviceplatform.tracking.manager.SocketFacade;

import io.kickflip.sdk.Kickflip;
import io.kickflip.sdk.api.KickflipCallback;
import io.kickflip.sdk.api.json.Response;
import io.kickflip.sdk.exception.KickflipException;

//import io.kickflip.sdk.Kickflip;

//import io.kickflip.sdk.Kickflip;
//import io.kickflip.sdk.api.json.Stream;
//import io.kickflip.sdk.av.BroadcastListener;
//import io.kickflip.sdk.exception.KickflipException;

/**
 * @author Waseem Mirza
 */

public class OlaApplication extends Application {

    private Config config;

    @Override
    public void onCreate() {
        super.onCreate();
        DevicePlatform.initialize(this, getAuthConfig(), new AnalyticsEventListener() {
            @Override
            public void dpSdkAnalyticsEvent(String s, Map<String, String> map) {
                Logger.e("dpSdkAnalyticsEvent event " + s + " attributes " + map);

            }

            @Override
            public void dpSdkAnalyticsFabricEvent(String s, int s1, String s2, Map<String, String> map) {
                Logger.e("dpSdkAnalyticsEvent event " + s+s1+s2 + " attributes " + map);
            }
        });


        SocketFacade.getInstance().setSocketCallback(new SocketCallback() {

            @Override
            public HashMap<String, Object> onBBMessageReceived(byte[] s) {
                Logger.e("Message received from Bumble bee: " + s);
                HashMap<String, Object> map = new HashMap<>();
                map.put("sample_ack", "sample_ack_value_from_BB");
                return map;
            }

            @Override
            public HashMap<String, String> onGoldBugMessageReceived(String s) {
                Logger.e("Message received from Goldbug: "  + s);
                //Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent("com.ola.Broadcast_BLDBG");
                intent.putExtra("message", s);
                sendBroadcast(intent);
                HashMap<String, String> map = new HashMap<>();
                map.put("sample_ack", "sample_ack_value_from_Goldbug");
                return map;
            }
        });

        setUpStreaming();
    }


    public Config getAuthConfig() {
        String sslKey = "30820122300d06092a864886f70d01010105000382010f003082010a02820101" +
                "00dc9d25e92663e85ebf7eeab1b8f85547e7e138211d3388095e497ae7bfbea1" +
                "04cb7f3fa3ce5d2beeec5d3a7332b5f1c96a668b7835a2d7a2507587ac8da94b" +
                "452b21a420c9abac3d2057b6505376ad486f3423750a97ba9f20cd7a6bd811f5" +
                "fd9ebdefddd969f115651e2e68c8660d78cae014d063f037eacc00bf412b71a5" +
                "862700fb6a8bc3cdbd8edbeebc484a1a87b91d2b86ae6254c9e3e4a012148d74" +
                "a550ce549985fdf5583f18e66538295f3f5ebffa45c317eb70177ddb5ba79b72" +
                "28445df169b065f1ca6ae59c7c8f7e528ba0db758b0e886eed26e47f6ae3403a" +
                "8125f125d141468157327ae66867101d3492ebe66f33296152ae37a0fae84696" +
                "110203010001";
        //BuildConfig.FLAVOR)?"https://devices.olacabs-dev.in/":"https://devices.olacabs.com/",sslKey,
        NetworkConfig config = new NetworkConfig(new OlaRetryPolicy(30000, 3), 1, new IAuthenticationListener() {
            @Override
            public void onAuthenticationChanged(AuthState authState) {
                onAuthenticationChanged(authState.getState());
            }


            public void onAuthenticationChanged(int i) {
                Logger.e("**** onAuthenticationChanged ****"+ i);
                String state;
                switch (i){
                    case AuthState.States.AUTHENTICATED:
                        state = "Authenticated";
                        break;
                    case AuthState.States.AUTH_IN_PROGRESS:
                        state = "UnAuthenticated";
                        break;
                    case AuthState.States.AUTH_DISABLED:
                        state = "Auth Disabled";
                        break;
                    case AuthState.States.AUTH_FORCE_DISABLED:
                        state = "Auth Force Disabled";
                        break;
                    case AuthState.States.DO_PREAUTH:
                        state = "Do PreAuth";
                        break;
                    default:
                        state = "Unknown";
                        break;
                }

                Logger.e("[AUTH-CALLBACK] ************************ Auth State changed: *********************************** :  " + state);
            }
        });
        AppConfig appConfig = new AppConfig(Constants.ClientAppId.DAPP, Constants.AppFlavor.STAGING,
                true);


        String OPTIMUS_STAGING_URL = "http://bumblebee-haproxy-stage-https-60564052.ap-southeast-1.elb.amazonaws.com:443";
        String OPTIMUS_PROD_URL = "http://bumblebees.olacabs.com:443";
        String STAGING_CLIENT_TYPE = "city_taxi";
        String PRODUCTION_CLIENT_TYPE = "0c69dd5d8ed74adeaeb6f85d26fc8e1d";
        String DEVICE_TYPE = "mobile";

        SocketConfig socketConfig = new SocketConfig();
        HashMap<String, String> map = new HashMap<>();
        map.put("type", "location");
        map.put("device_type", "mobile");
        map.put("device_category", "luxury_sedan");
        map.put("device_categories", "luxury_sedan");
        map.put("device_status", "110");
        map.put("city", "pune");
        map.put("version_code","82911");
        map.put("device_active", null);
        map.put("device_source", "city_taxi");


        socketConfig.setTrackingData(map);
        return new Config.Builder().setAppConfig(appConfig).setNetworkConfig(config).setSocketConfig(socketConfig).build();
    }


   void  setUpStreaming() {
       Kickflip.setup(this, ";p;uO@nlSFpvhiLgaglTjkfGLr6TPB5Sz3EGxhEl", "4GNOTr-ZR;r3L=Z==Ai.jZzrbgLY?zrZRx.HRqGNklpD307?Gvx5T:YxcK=eQut9C!YyisGOi?BaMx:Vuumol?rFy7ZpaqnYR4Xo!EfnxYgMBAj@Z@a.oTocYEENfiAn", new KickflipCallback() {
           @Override
           public void onSuccess(Response response) {
               Logger.e("HIMANSHU Kickflip initilised success ****  "+response.getReason());
           }

           @Override
           public void onError(KickflipException error) {
               Logger.e("HIMANSHU Kickflip initilised failed****  "+ error.toString());
           }
       });
   }


}
