package com.ola.consumer.map;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ola.sdk.deviceplatform.common.base.utils.Logger;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import mytaxi.com.ola.R;

/**
 * Created by Himanshu on 21/02/18.
 */

public class MapUtil implements GoogleMap.OnMapClickListener{
    private final OnUserActionListener mListener;
    private GoogleMap map;
    private ArrayList<LatLng> markerPoints;

    private ArrayList<LatLng> checkpoints;
    private Set<String> route;

    private ArrayList<LatLng> mSafePath;

    public MapUtil(GoogleMap map, OnUserActionListener lisnter) {
        this.map = map;
        this.mListener = lisnter;
        // Initializing array List
        markerPoints = new ArrayList<LatLng>();
    }

    public void chooseRoute(LatLng mPickupPoint, LatLng mDropPoint) {
        MarkerOptions options = new MarkerOptions();

        options.position(mPickupPoint);
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        map.addMarker(options);

        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(12.995445, 77.578742)));
        map.animateCamera(CameraUpdateFactory.zoomTo(13));

        map.setOnMapClickListener(null);


        options = new MarkerOptions();
        options.position(mDropPoint);
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        map.addMarker(options);


        String url = getDirectionsUrl(mPickupPoint, mDropPoint);

        DownloadTask downloadTask = new DownloadTask(map, this);

        downloadTask.execute(url);

    }

    @Override
    public void onMapClick(LatLng point) {


        checkpoints = null;
        if(route == null){
            route = new HashSet<>();
        }
        route.clear();

        if(markerPoints.size()>1){
            markerPoints.clear();
            map.clear();
        }
        if(mListener != null) {
            mListener.updateRoute(route);
            mListener.updateCheckpoints(checkpoints);
        }

        markerPoints.add(point);

        MarkerOptions options = new MarkerOptions();

        options.position(point);

        if(markerPoints.size()==1){
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        }else if(markerPoints.size()==2){
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        map.addMarker(options);

        if(markerPoints.size() >= 2){
            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);

            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask(map, this);

            downloadTask.execute(url);
        }
    }




//    public ArrayList<LatLng> getCheckpoints() {
//        return checkpoints;
//    }

    public interface OnUserActionListener {
        void updateRoute(Set<String> route);
        void updateCheckpoints(ArrayList<LatLng> checpoints);
        void onBookingReady();

        void noRouteFound();
    }


    public void init() {


        //map.setMyLocationEnabled(true);

        map.setOnMapClickListener(this);
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        String alternatives = "alternatives=true";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor+"&"+alternatives;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    private static String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Logger.e("DOWNLOADING PATH ******");
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

//    public Set<String> getPath() {
//        return route;
//    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        private GoogleMap map;
        private MapUtil util;


        DownloadTask(GoogleMap map, MapUtil util) {
            this.map = map;
            this.util = util;
        }

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Logger.e("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Logger.e("EXECUTING LOGGER TASK ***");
            ParserTask parserTask = new ParserTask(map, util);

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>>> {

        private GoogleMap map;

        private MapUtil util;


        private int safeRoute = 0;

        private ArrayList<LatLng> safePath;

        private ArrayList<LatLng> dangerPoints = new ArrayList<>();


        ParserTask(GoogleMap map, MapUtil util) {
            this.map = map;
            this.util = util;
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            if(result == null){
                return;
            }

            if( result.size() > 1){
                safeRoute = getSafeRoute(result);
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);
                int pos = 19;
                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    pos++;
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    if(safeRoute != i && j != path.size()-1) {
                        if(pos%19 == 0) {
                            dangerPoints.add(position);
                        }
                    }

                    if(j == path.size()/2) {
                        MarkerOptions options = new MarkerOptions()
                                .flat(true)
                                .position(position);
                        if(safeRoute != i) {
                            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_danger));
                            options.title("Safety Score " + String.format("%.2f", (.20 * (i + 1))));
                        } else {
                            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_green));
                            options.title("Safety Score " + String.format("%.2f", (.99 - (i * .03))));
                        }
                        map.addMarker(options);
                    }


                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                if(i == safeRoute) {
                    safePath = points;
                    lineOptions.color(Color.parseColor("#388E3C"));
                } else {
                    lineOptions.color(Color.parseColor("#F44336"));
                }

                map.addPolyline(lineOptions);
            }

            if(safePath != null && safePath.size()>0) {
                lineOptions = new PolylineOptions();
                lineOptions.addAll(safePath);
                lineOptions.width(20);
                lineOptions.color(Color.parseColor("#388E3C"));
                map.addPolyline(lineOptions);
            }


            addDangerPoints(dangerPoints);

            route = getRoute(safePath);
            checkpoints = computeCheckpoints(safePath);
            mSafePath = safePath;
            if(safePath != null && safePath.size()>0){
                mListener.onBookingReady();

            } else {
                mListener.noRouteFound();
                map.setOnMapClickListener(util);
            }

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    map.clear();
//                    if(safePath != null && safePath.size()>0) {
//                        PolylineOptions lineOptions = new PolylineOptions();
//                        lineOptions.addAll(safePath);
//                        lineOptions.width(20);
//                        lineOptions.color(Color.parseColor("#388E3C"));
//                        map.addPolyline(lineOptions);
//                        checkpoints = computeCheckpoints(safePath);
//
//                        route = getRoute(safePath);
//                        mListener.updateRoute(route);
//                        mListener.updateCheckpoints(checkpoints);
//
//                    }
//                }
//            }, 3500);


            // Drawing polyline in the Google Map for the i-th route
            //map.addPolyline(lineOptions);
        }

        private int getSafeRoute(List<List<HashMap<String, String>>> result) {

            ArrayList<String> prePoints = new ArrayList<>();

            Set<String> currPath = new HashSet<>();

            prePoints.add((13.01879+","+77.55756));
            prePoints.add((13.01476+","+77.57446));
            prePoints.add((13.00729+","+77.5924));
            prePoints.add((12.99358+","+77.59762));
            prePoints.add((12.98142+","+77.59597));

            int res = 0;

            int count = 0;

            if (result != null && result.size() > 0)

                for(int i=0;i<result.size();i++) {
                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);
                    currPath.clear();
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String,String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        currPath.add(lat+","+lng);

                    }

                    for(String checkPoint: prePoints){
                        if(currPath.contains(checkPoint)){
                            count++;
                            if(count == prePoints.size()){
                                return i;
                            }
                        } else {
                            break;
                        }
                    }


                }
            return res;
        }

        private Set<String> getRoute(ArrayList<LatLng> safePath) {
            Set<String> res = new HashSet<>();
            if(safePath != null && safePath.size() > 0){
                for(LatLng pos: safePath) {
                    res.add(String.format("%.3f", pos.latitude)+","+ String.format("%.3f", pos.longitude));
                }
            }
            return res;
        }

        private void addDangerPoints(ArrayList<LatLng> dangerPoints) {
            int factor = 1;
            if(dangerPoints != null && dangerPoints.size() > 0) {
                if(dangerPoints.size() > 5){
                    factor = dangerPoints.size()/5;
                }
                int pointNo = 1;
                for(LatLng pos:dangerPoints) {
                    if(pointNo%factor == 0) {
                        MarkerOptions options = new MarkerOptions()
                                .flat(true)
                                .position(pos);
                        //ic_error.pngoptions.title("Danger Points");
                        //options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_error));

                        map.addMarker(options);
                    }
                    pointNo++;
                }
            }
        }


        private ArrayList<LatLng> computeCheckpoints(ArrayList<LatLng> pathPoints) {
            ArrayList<LatLng> checkpoints = new ArrayList<LatLng>();
            int factor = 1;
            if(pathPoints != null && pathPoints.size() > 0) {
                if(pathPoints.size() > 5){
                    factor = pathPoints.size()/5;
                }
                int pointNo = 1;
                for(LatLng pos:pathPoints) {
                    if(pointNo % factor == 0) {
                        MarkerOptions options = new MarkerOptions()
                                .flat(true)
                                .position(pos);
                        options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_checkpoint));

                        map.addMarker(options);
                        checkpoints.add(pos);
                    } pointNo++;
                }

                checkpoints.add(pathPoints.get(pathPoints.size()-1));
            }
            return checkpoints;
        }



    }


    public void confirmBooking() {
        map.clear();
        if(mSafePath != null && mSafePath.size()>0) {
            PolylineOptions lineOptions = new PolylineOptions();
            lineOptions.addAll(mSafePath);
            lineOptions.width(20);
            lineOptions.color(Color.parseColor("#388E3C"));
            map.addPolyline(lineOptions);

            mListener.updateRoute(route);
            mListener.updateCheckpoints(checkpoints);

        }

        for(LatLng pos:checkpoints) {
            MarkerOptions options = new MarkerOptions()
                    .flat(true)
                    .position(pos);
            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_checkpoint));

            map.addMarker(options);
        }
    }


}
