package com.ola.consumer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.ola.consumer.services.DemoCamService;
import com.ola.sdk.deviceplatform.common.base.utils.Logger;

import java.util.concurrent.TimeUnit;

public class GoldBugReceiver extends BroadcastReceiver {

    private static final String STREAM_VIDEO = "stream-video";
    private static final String CAPTURE_IMAGE= "capture-image";

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
       if(intent != null){
           String msg = intent.getStringExtra("message");
           if(!TextUtils.isEmpty(msg)) {
               if(msg.equalsIgnoreCase(CAPTURE_IMAGE)){
                   Logger.e("HIMANSHU", "GoldBugReceiver message received ** "+CAPTURE_IMAGE);
                   if(context.startService(new Intent(context, DemoCamService.class))!= null){
                       Logger.e("HIMANSHU", "DemoCamService started "+CAPTURE_IMAGE);
                   }
               } else if (msg.equalsIgnoreCase(STREAM_VIDEO)){

                   Intent in = new Intent("com.android.RECORD_VIDEO");
                   in.putExtra("message", msg);
                   Logger.e("HIMANSHU", "DemoCamService started "+CAPTURE_IMAGE);
                   context.sendBroadcast(in);
               }
           }
       }
    }
}
