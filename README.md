# Ola App


## Getting Started

The project was created on Android Studio 3.0.1 with android gradle plugin version 3.0.1

### Prerequisites

Need Android Studio 3.0.1 with android gradle plugin version 3.0.1

### Installing

Navigate to project root directory and execute following command

```
./gradlew clean build
```

The above command should generate debug and release APKs under build directory

## Deployment

The phone should have an updated version on play services to run the application.

## Built With

* [Android Studio](https://developer.android.com/studio/index.html) - The Android IDE
* [Gradle](https://gradle.org/) - Dependency Management and Build Tool

## Authors

* **Waseem Mirza**

## Acknowledgments

* Used Retrofit 2 library